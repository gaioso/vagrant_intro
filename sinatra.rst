Iniciando el proyecto Sinatra
=============================

Iniciamos la máquina invitada con el comando::

   $ vagrant up

Para acceder a la aplicación a través del navegador anfitrión, debemos iniciar el servidor HTTP con los comandos siguientes::

   $ vagrant ssh
   $ ruby app.rb -o 0.0.0.0 &
   $ logout

Si hemos finalizado nuestro trabajo para la jornada de hoy, debemos parar la máquina mediante el comando::

   $ vagrant halt

Cuando necesitemos trabajar de nuevo con esta máquina, la volvemos a arrancar::

   $ vagrant up

Si no tenemos acceso a nuestra página en el sistema invitado, debemos iniciar de nuevo el servidor HTTP.

Ahora, procedemos a la parada de la máquina, pero con el siguiente comando::

   $ vagrant suspend

En este momento, el estado de la máquina es 'saved', y no consume memoria RAM en el sistema anfitrión. Para volver a iniciar el sistema, ejecutamos uno de los siguientes comandos::

   $ vagrant up
   $ vagrant resume

Modificar el puerto
-------------------

Si el puerto 45670 está ocupado en nuestro sistema anfitrión, debemos realizar un cambio en el fichero Vagrantfile. Por ejemplo, podemos escribir la siguiente línea::

   config.vm.network :forwarded_port, guest: 4567, host: 9090, host_ip: "127.0.0.1"

Aplicamos los cambios con el comando::

   $ vagrant reload

Eliminar la imagen
------------------

Para visualizar las imágenes descargadas durante la ejecución de los comandos ``$ vagrant up``, usamos el comando::

   $ vagrant box list

Cada imagen, puede ser eliminada del sistema con el siguiente comando::

   $ vagrant box remove NOMBRE

