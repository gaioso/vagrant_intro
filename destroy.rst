Destruír las máquinas virtuales
===============================

Cuando hayamos acabado de realizar el trabajo con entorno servidor, podemos destruír las máquinas, o bien, detenerlas::

   $ vagrant halt [ID]
   $ vagrant destroy [ID]

Los [ID] podemos consultarlos en la salida de ``$ vagrant global-status``, o bien, situarnos dentro del directorio donde está ubicado en fichero Vagrantfile, que es donde se ejecutó el comando de creación de la MV ``$ vagrant up``.
