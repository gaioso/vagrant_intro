Framework Web
=============

En este apartado veremos a Vagrant en acción, a través de la configuración de una aplicación web a través de un framework, en este caso, con AngularJS (Javascript).

Nuestro ejemplo hará uso del puerto TCP 8800, por lo que debe estar libre en nuestro equipo anfitrión.

Songs for kids
--------------

Este proyecto, que está escrito en AngularJS, está formado por tres páginas, cada una de ellas muestra el texto de una canción infantil en Inglés. Para poder ejecutar el proyecto debemos ejecutar los siguientes comandos::

   $ cd directorio/para/ejercicios
   $ git clone https://github.com/gaioso/songs-app-angularjs.git
   $ cd songs-app-angularjs
   $ vagrant up
   ...
   # Run webbrowser and visit http://localhost:8800/

Dependiendo de la conexión de nuestro equipo, el proceso completo puede tardar varios minutos. Una vez finalizado, podremos acceder a los contenidos del proyecto a través de la URL que aparece en la última línea [http://localhost:8800/].

En este instante, hay dos sistemas operativos ejecutandose en nuestro equipo:

* Anfitrión (host): El sistema operativo principal.
* Invitado (guest): El sistema operativo iniciado mediante $ vagrant up.

Podemos comprobarlo a través de la interfaz de administración de VirtualBox, o bien con el siguiente comando::

   $ vagrant global-status

La aplicación web está siendo servida por un servidor HTTP que se ejecuta dentro del sistema operativo invitado: Ubuntu 14.04. El código fuente de la aplicación está disponible tanto para el SO anfitrión como para el invitado a través de un directorio compartido songs-app-angularjs/web/api/song/. Usando un editor de texto podemos modificar alguno de los ficheros json del directorio y comprobar los cambios a través del navegador del sistema anfitrión.

Ejercicio
~~~~~~~~~

Realizar las mismas operaciones que en el ejemplo anterior, pero con el código disponible en el siguiente repositorio::

   https://github.com/gaioso/songs-app-rails

¿Qué hemos conseguido?
----------------------

Para poder ejecutar el primer ejemplo en nuestro sistema anfitrión sin hacer uso de la virtualización, tendríamos que instalar y configurar un servidor web (Apache, por ejemplo). Aunque no es una tarea complicada, requiere de un proceso manual y dependiente del sistema operativo que estemos usando. Del mismo modo que se mencionaba más arriba, esta tarea es responsabilidad de un perfil técnico, y un desarrollador no tiene porque saber instalar y/o configurar un servidor web para usar correctamente su aplicación.

