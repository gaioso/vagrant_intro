Paradigma cliente-servidor en aplicaciones web.
===============================================

Las aplicaciones web utilizan el esquema cliente/servidor, haciendo uso del protocolo HTTP para la gestión de la comunicación. Cuando ejecutamos una aplicación web, existen dos partes bien diferenciadas: el cliente, que es el proceso que envía las peticiones; y el servidor, que el proceso responsable de responder.  Dichas aplicaciones suelen estar en diferentes máquinas, equipadas con diferente hardware y sistema operativo. El cliente se puede estar ejecutando en un equipo portátil, y el servidor es el proceso que se ejecuta en una máquina remota.

.. image:: ./images/Fig01-01.jpg

Los dos componentes de una aplicación web suelen denominarse como **front end** y **back end**. El **front end** es ejecutado en un navegador web en el lado del cliente; y el **back end** se ejecuta en el lado del servidor.
