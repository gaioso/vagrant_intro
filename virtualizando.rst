Virtualizando la solución
=========================

Cuando trabajamos con MVs, podemos disponer del «front end» y del «back end» en la misma máquina; el equipo del desarrollador. El proceso cliente se ejecuta de igual forma que en el esquema anterior, como un proceso normal en el sistema operativo del equipo. El lado del servidor, es el que virtualizamos en una MV. Aunque ambos entornos se están ejecutando en la misma máquina, podemos reproducir el comportamiento real gracias a la virtualización.

.. image:: ./images/Fig01-06.jpg

Cuando trabajamos de este modo, tendremos que gestionar dos sistemas operativos. El original, instalado en la máquina del desarrollador, al que llamaremos «SO anfitrión» (host), y el que están instalado en la MV, que serán el «SO invitado» (guest).
