.. Vagrant documentation master file, created by
   sphinx-quickstart on Wed Aug 30 08:03:22 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentación Vagrant
=====================

.. toctree::
   :maxdepth: 2

   presentacion
   paradigma
   virtualizando
   vagrant
   framework
   destroy
   estados
   ficheros
   sinatra
   atlas
   seguridad
   primerabox
   aprovisionamiento
   imagenes

Licencia
--------

.. toctree::
   :maxdepth: 1

   licencia

