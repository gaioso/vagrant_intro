Los estados de una Máquina Virtual
==================================

Comenzaremos por analizar las acciones involucradas en la ejecución de $ vagrant up, y las agruparemos en tres etapas. Prestaremos atención a varios ficheros y directorios que están involucrados en el arranque así como el momento en el que se inicia cada una de las etapas. Luego, analizaremos los cinco estados en los que se puede encontrar un sistema operativo invitado.

Haremos uso de un ejemplo similar al anterior, pero en esta ocasión, realizado con el framework Sinatra.

**Antes de empezar**

Comprobamos que no se están ejecutando otras MVs en nuestro equipo::

   $ vagrant global-status

**Descarga del código fuente de la aplicación**

Para poder realizar el ejercicio debemos descargar el código fuente, que se encuentra disponible en https://github.com/gaioso/songs-app-sinatra/::

   $ git clone https://github.com/gaioso/songs-app-sinatra.git

Vagrantfile
-----------

La configuración de las MVs usadas en nuestros ejercicios está guardada en el fichero Vagrantfile. Cada proyecto tiene su propio fichero, donde se guardan las propiedades de la MV. Este fichero está escrito en Ruby, y contiene, entre otras cosas, el nombre de la 'box' inicial desde la que construír la MV.

Como el fichero *Vagrantfile* almacena los detalles de la configuración de la MV, cualquier desarrollador que tenga acceso a dicho fichero, podrá acceder al mismo entorno que el resto de compañeros.

El fichero Vagrantfile de nuestro proyecto actual 'songs-app-sinatra' está configurado como Version 2, y está formado por dos instrucciones::

   config.vm.box = "http://boxes.gajdaw.pl/sinatra/sinatra-v1.0.0.box"
   config.vm.network :forwarded_port, guest: 4567, host: 45670, host_ip: "127.0.0.1"

Imagen de una Máquina Virtual
-----------------------------

La imagen usada por Vagrant para crear y ejecutar el sistema invitado está formada por un único fichero, al que se suele denominar como «box file», o fichero imagen. La extensión usada para este tipo de fichero es .box, pero no es obligatorio.

Iniciando una Máquina Virtual
-----------------------------

Cuando se ejecuta el comando $ vagrant up la primera vez, veremos una salida similar a la siguiente::

   1 Bringing machine 'default' up with 'virtualbox' provider...
   1==> default: Box 'http://boxes.gajdaw.pl/sinatra/sinatra-v1.0.0.box' could not be found. Attempting to find and install...
   1    default: Box Provider: virtualbox
   1    default: Box Version: >= 0
   1==> default: Adding box 'http://boxes.gajdaw.pl/sinatra/sinatra-v1.0.0.box' (v0) for provider: virtualbox
   1    default: Downloading: http://boxes.gajdaw.pl/sinatra/sinatra-v1.0.0.box
   1==> default: Box download is resuming from prior download progress
   1==> default: Successfully added box 'http://boxes.gajdaw.pl/sinatra/sinatra-v1.0.0.box' (v0) for 'virtualbox'!
   2==> default: Importing base box 'http://boxes.gajdaw.pl/sinatra/sinatra-v1.0.0.box'...
   3==> default: Matching MAC address for NAT networking...
   3==> default: Setting the name of the VM: songs-app-sinatra_default_1425397277271_42545
   3==> default: Clearing any previously set network interfaces...
   3==> default: Preparing network interfaces based on configuration...
   3    default: Adapter 1: nat
   3==> default: Forwarding ports...
   3    default: 4567 => 45670 (adapter 1)
   3    default: 22 => 2222 (adapter 1)
   3==> default: Booting VM...
   3==> default: Waiting for machine to boot. This may take a few minutes...
   3    default: SSH address: 127.0.0.1:2222
   3    default: SSH username: vagrant
   3    default: SSH auth method: private key
   3    default: Warning: Connection timeout. Retrying...
   3    default:
   3    default: Vagrant insecure key detected. Vagrant will automatically replace
   3    default: this with a newly generated keypair for better security.
   3    default:
   3    default: Inserting generated public key within guest...
   3    default: Removing insecure key from the guest if its present...
   3    default: Key inserted! Disconnecting and reconnecting using new SSH key...
   3==> default: Machine booted and ready!
   3==> default: Checking for guest additions in VM...
   3==> default: Mounting shared folders...
   3    default: /vagrant => /examples/songs-app-sinatra
   3==> default: Running provisioner: shell...
   3    default: Running: inline script
   3==> default: stdin: is not a tty

El proceso de arranque de la máquina está dividido en tres etapas:

* Etapa 1. Descarga e instalación de la 'box' en el sistema anfitrión. ``~/.vagrant.d/boxes``
* Etapa 2. Importación de la 'box' al proyecto. ``~/VirtualBox VMs/``
* Etapa 3. Arranque del sistema.

El nombre del directorio en el cual Vagrant almacena las imágenes descargadas, es el mismo que la URL, pero con el carácter ``/`` sustituído por ``-VAGRANTSLASH-``.

Una vez finalizado el proceso de arranque de la nueva máquina virtual, se devuelve el control al prompt de nuestro sistema. No se aprecia ningún cambio, ya que la máquina se ha iniciado en modo 'headless', y no disponemos de interfaz GUI. Para comprobar el estado actual de la máquina recién iniciada, ejecutamos el siguiente comando::

   $ vagrant status

En este directorio existe un subdirectorio oculto con nombre '.vagrant', que es donde se almacena el ID y otra información sobre el sistema invitado. Está preparado para ser creado con el comando $ vagrant up y eliminado con $ vagrant destroy.

