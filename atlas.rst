Atlas
=====

Para poder crear un nuevo proyecto que haga uso de un sistema operativo invitado, necesitamos una imagen con un sistema preinstalado. Esta imagen, que se denomina «base box» en la terminología Vagrant, puede ser creada desde cero o descargada desde Internet.

De momento, seguiremos utilizando imágenes descargadas desde Internet, y más adelante veremos como crearlas desde un CDROM, por ejemplo.

El Vagrantfile que se ha utilizado en los ejemplos anteriores, contiene una línea similar a la siguiente::

   config.vm.box = "http://boxes.gajdaw.pl/sinatra/sinatra-v1.0.0.box"

Cuando se inicia la máquina, Vagrant descarga el fichero que aparece en la URL. A partir de la versión 1.6, Vagrant puede hacer uso de un servicio 'cloud' conocido como **Atlas** para descargar 'base boxes'. Este servicio, está disponible en https://atlas.hashicorp.com. Cada imagen se identifica por un nombre que está formado por dos partes: el nombre del fabricante y el nombre del 'box'.

Por ejemplo, el nombre *hashicorp/precise64* se refiere al 'box' creado por Hashicorp y que tiene como nombre *precise64*. Los detalles de este 'box' se puede consultar en https://atlas.hashicorp.com/hashicorp/boxes/precise64. Si queremos buscar otras imágenes diferentes, debemos escribir su nombre en la dirección https://atlas.hashicorp.com/boxes/search.

Iniciar un nuevo proyecto
-------------------------

El comando Vagrant que usaremos para iniciar un nuevo proyecto es ``$ vagrant init``. Por ejemplo, para crear un nuevo proyecto que haga uso de la box ubuntu/trusty32, ejecutamos los siguientes comandos::

   $ cd directorio/con/ejemplos
   $ mkdir nuevo-proyecto
   $ cd nuevo-proyecto
   $ vagrant init -m ubuntu/trusty32

El objetivo del comando ``$ vagrant init`` es la creación del fichero de configuración *Vagrantfile*. La opción ``-m`` hace que el fichero *Vagrantfile* tenga un contenido mínimo. En caso de no utilizar esta opción, dicho fichero contendrá multitud de líneas comentadas con diferentes opciones de configuración. Otra opción de este comando que nos puede resultar útil, es ``-f``, que fuerza la creación del fichero *Vagrantfile* aún cuando exista una versión anterior.
